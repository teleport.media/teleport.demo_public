const { CheckerPlugin } = require('awesome-typescript-loader');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')
 
const HMR = process.argv.join('').indexOf('hot') > -1;
const NODE_ENV = process.env.NODE_ENV || 'develop';
const DEVELOP = NODE_ENV !== 'production';

const cssLoader = {
  loader: 'css-loader',
  options: {
    minimize: !DEVELOP
  }
};

module.exports = {
  entry: {
    app: ['./src/public.ts'],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js',
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loader: HMR ? ['style-loader', 'css-loader', 'sass-loader'] : ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            cssLoader,
            'sass-loader'
          ]
        })
      },
      {
        test: /\.(ttf|eot|woff|woff2|svg)$/,
        loader: 'file-loader',
        options: {
          name: '/fonts/[hash].[ext]'
        }
      },
    ]
  },
  plugins: [
    new CheckerPlugin(),
    new CopyWebpackPlugin([
      {
        from: 'static/js',
        to: 'js'
      },
      {
        from: 'static/html/index.html',
        to: './'
      }
    ]),
    new ExtractTextPlugin('css/[name].css'),
  ]
};
